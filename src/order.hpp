#ifndef ORDER_HPP
#define ORDER_HPP

#include <QObject>
#include <QList>

#include <string>

#include "salesitem.hpp"

/**
 * @brief The Order class
 * This class holds a full customer Order, as well as customer information
 * The class extends QObject to export QProperties and be able to
 * be viewed within the QT user interface.
 */
class Order : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString customerOrderID READ customerOrderID)
    Q_PROPERTY(QString customerName READ customerName)
    Q_PROPERTY(QString customerEmail READ customerEmail)
    Q_PROPERTY(QString customerPhone READ customerPhone)
    Q_PROPERTY(QString orderStatus READ orderStatus)
    Q_PROPERTY(QList<SalesItem*> orderItems READ orderItems)
    Q_PROPERTY(QString orderSummary READ orderSummary)
    Q_PROPERTY(double totalPrice READ totalPrice)


public:
    Order(QString id, QString customerName, QString customerEmail, QString customerPhone, QString orderStatus, QList<SalesItem*> orderItems);
    QString customerOrderID() const;
    QString customerName() const;
    QString customerEmail() const;
    QString customerPhone() const;
    QString orderStatus() const;
    QList<SalesItem*> orderItems() const;
    QString orderSummary() const;
    double totalPrice() const;

private:
    QString m_id;
    QString m_customerName;
    QString m_customerEmail;
    QString m_customerPhone;
    QString m_orderStatus;
    QList<SalesItem*> m_orderItems;
};

#endif // ORDER_HPP

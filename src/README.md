
# El Nene's Cookies Backend Management Program

## Author of Folder
* Liam Jensen (besides json and cURLpp libraries)

## Dependencies
* qt5 (qmake)
* \>=C++17 compiler

## Running
* Run `qmake` within the `cs4800-project/src` directory.
* Run `cookiesales` from the `bin` directory.
* If the backend API or mongoDB is not running, a backup `orders.json` file will populate the _Unfulfilled Orders_ screen.
  * See the `cs4800-project/python` directory for information on running the API and associated mongoDB instance.

import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Item {
    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        Text {
            y: 0
            text: "Inventory"
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillHeight: false
            Layout.fillWidth: true
            font.underline: true
            anchors.leftMargin: 5
            anchors.topMargin: 15
            font.pixelSize: 24
        }

        ListView {
            x: 0
            y: 0
            Layout.margins: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.topMargin: 0
            layer.textureMirroring: ShaderEffectSource.MirrorVertically
            clip: true
            cacheBuffer: 320
            pixelAligned: false
            boundsBehavior: Flickable.StopAtBounds
            enabled: true
            ScrollBar.vertical: ScrollBar {
                id: scroll
                policy: ScrollBar.AlwaysOn
            }

            model: inv

            delegate: RowLayout {
                spacing: 5
                anchors.left: parent.left
                width: parent.width - scroll.width // Scrolbar displays on top of view otherwise
                height: 50

                Text {
                    y: 0
                    font.pixelSize: 20
                    text: name
                    Layout.alignment: Qt.AlignLeft
                    anchors.leftMargin: 0
                }

                RowLayout {
                    spacing: 5
                    Layout.alignment: Qt.AlignRight

                    Text {
                        x: 0
                        y: 0
                        font.pixelSize: 20
                        text: amount
                        Layout.alignment: Qt.AlignLeft
                        anchors.rightMargin: 0
                    }

                    TextInput {
                        Layout.alignment: Qt.AlignRight
                        width: 30
                        clip: false
                    }
                }
            }
        }
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:3}D{i:1}
}
##^##*/

#ifndef API_H
#define API_H

#include <string>
#include <iostream>
#include <sstream>

#include "order.hpp"
#include "json.hpp"

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

// Handle Order API connection
class API {

public:
    // Return all unfullfilled DB orders
    static QList<QObject*> getUnfulfilledOrders();

    // Return all fulfilled DB orders
    static QList<QObject*> getPastOrders();

    // Return store inventory
    static QList<QObject*> getInventory();

    // Sets Order to Fulfilled in API
    static bool setOrderFulfilled(std::string orderID);

    // Sets Order to Cancelled in API
    static bool setOrderCancelled(std::string orderID);

    // TODO: Remove or move helper fn
    static QString fromStr(const std::string str) {
        return QString::fromStdString(str);
    }

private:
    // API url variables
    inline static std::string baseURL = "http://testingbakersear.herokuapp.com/api/";
    inline static std::string allOrdersURL = baseURL + "get_orders";
    inline static std::string getInventoryURL = baseURL + "get_inventory";
    inline static std::string updateOrderURL = baseURL + "update_order";
    inline static std::string updateInventoryURL = baseURL + "update_inventory";

    /* Performs an HTTP request on given url
     * and returns a json representation of the result
     */
    static nlohmann::json performHttpRequest(const std::string url) {
        std::stringstream os;
        curlpp::Cleanup myCleanup;
        curlpp::Easy easyhandle;

        // CURL params
        easyhandle.setOpt(cURLpp::Options::Url(url));
        //easyhandle.setOpt(cURLpp::Options::HttpGet());
        easyhandle.setOpt(curlpp::options::WriteStream(&os));

        easyhandle.perform();

        // Returned request to json
        nlohmann::json j = nlohmann::json::parse(os.str());

        curlpp::terminate();

        return j;
    }

    static bool performPOSTRequest(const std::string url, const nlohmann::json jsonBody) {
        curlpp::Easy easyhandle;
        std::stringstream os;

        std::list<std::string> header;
        header.push_back("Content-Type: application/json");
        std::string body = jsonBody.dump();

        easyhandle.setOpt(cURLpp::Options::Url(url));
        easyhandle.setOpt(new curlpp::options::HttpHeader(header));

        easyhandle.setOpt(new curlpp::options::PostFields(body));
        easyhandle.setOpt(new curlpp::options::PostFieldSize(body.length()));
        easyhandle.setOpt(curlpp::options::WriteStream(&os));
        easyhandle.perform();
        std::cout << os.str() << std::endl;

        return true;
    }

    // Get all orders in json representation
    static nlohmann::json getOrdersJson() {
        return performHttpRequest(API::allOrdersURL);
    }

    // Get full inventory json
    static nlohmann::json getInventoryJson() {
        return performHttpRequest(API::getInventoryURL);
    }

    // Parses JSON element to Order object
    static Order* fromJsonElement(nlohmann::json element);
};

#endif // API_H

import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Item {
    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        Text {
            y: 0
            text: "Settings"
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillHeight: false
            Layout.fillWidth: true
            font.underline: true
            anchors.leftMargin: 5
            anchors.topMargin: 15
            font.pixelSize: 24
        }

        ListView {
            x: 0
            y: 0
            Layout.margins: 0
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.topMargin: 0
            layer.textureMirroring: ShaderEffectSource.MirrorVertically
            clip: true
            cacheBuffer: 320
            pixelAligned: false
            boundsBehavior: Flickable.StopAtBounds
            enabled: true
            ScrollBar.vertical: ScrollBar {
                id: scroll
                policy: ScrollBar.AlwaysOn
            }

            model: items

            delegate: RowLayout {
                spacing: 5
                anchors.left: parent.left
                width: parent.width - scroll.width // Scrolbar displays on top of view otherwise

                Text {
                    y: 0
                    font.pixelSize: 20
                    text: name
                    Layout.alignment: Qt.AlignLeft
                    anchors.leftMargin: 0
                }

                Text {
                    x: 0
                    y: 0
                    font.pixelSize: 20
                    text: price
                    Layout.alignment: Qt.AlignRight
                    anchors.rightMargin: 0
                }
            }
        }
    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:3;anchors_height:100;anchors_width:100}
D{i:1;anchors_height:172;anchors_width:279;anchors_x:0;anchors_y:224}
}
##^##*/

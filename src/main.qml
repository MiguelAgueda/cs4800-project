import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11


Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("El Nenes Cookies")

    property var unfulfilled_orders_view: "unfulfilled_orders";
    property var past_orders_view:        "past_orders";
    property var inventory_view:          "inventory";
    property var settings_view:           "settings";

    Loader {
        id: pageLoader
        width: 0
        height: 0
        anchors.left: column.right
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.leftMargin: 0
        sourceComponent: window_pane
        Component.onCompleted: loadView(unfulfilled_orders_view) // Initial View
    }


    function loadView(view) {
        switch(view) {
        case unfulfilled_orders_view:
            console.log("Unfulfilled")
            console.log(api.getUnfulfilledOrders)
            break
        case past_orders_view:
            console.log("past")
            console.log(api.getPastOrders)
            break
        case inventory_view:
            console.log("Inv")
            console.log(api.getInventory)
            break
        }

        unfulfilledOrders = api.getUnfulfilledOrders
        pageLoader.source = view + ".qml"
    }
    
    ColumnLayout {
        id: column
        width: unfulfilledOrdersButton.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Button {
            id: unfulfilledOrdersButton
            text: qsTr("Unfulfilled Orders")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(unfulfilled_orders_view)
        }

        Button {
            id: pastOrdersButton
            text: qsTr("Past Orders")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(past_orders_view)
        }

        Button {
            id: inventoryButton
            text: qsTr("Inventory")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(inventory_view)
        }

        Button {
            id: settingsButton
            text: qsTr("Settings")
            Layout.fillHeight: true
            Layout.fillWidth: true
            anchors.leftMargin: 0
            anchors.topMargin: 0

            onClicked: loadView(settings_view)
        }
    }

    Component {
        id: window_pane
        Pane {
            width: 0
            height: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.left: column.right
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
        }
    }

}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}D{i:4;anchors_height:400;anchors_x:0;anchors_y:20}
}
##^##*/

#ifndef APIQT_H
#define APIQT_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "api.hpp"

class APIQtWrapper : public QObject {
    Q_OBJECT
    Q_PROPERTY(bool getUnfulfilledOrders READ getUnfulfilledOrders)
    Q_PROPERTY(bool getPastOrders READ getPastOrders)
    Q_PROPERTY(bool getInventory READ getInventory)

public:
    // engine kept as member to update view objects with API
    APIQtWrapper(QQmlApplicationEngine* engine) {
        this->engine = engine;
    };
    ~APIQtWrapper() {};

    // Sets QML root context object to unfulfilled orders
    bool getUnfulfilledOrders() const {
        this->engine->rootContext()->setContextProperty("unfulfilledOrders", QVariant::fromValue(API::getUnfulfilledOrders()));
        return true;
    }

    bool getPastOrders() const {
        this->engine->rootContext()->setContextProperty("pastOrders", QVariant::fromValue(API::getPastOrders()));
        return true;
    }

    bool getInventory() const {
        this->engine->rootContext()->setContextProperty("inv", QVariant::fromValue(API::getInventory()));
        return true;
    }

    Q_INVOKABLE bool setOrderFulfilled(QString orderID) {
        return API::setOrderFulfilled(orderID.toStdString());
    }

    Q_INVOKABLE bool setOrderCancelled(QString orderID) {
        return API::setOrderCancelled(orderID.toStdString());
    }

private:
    QQmlApplicationEngine* engine;

};
#endif // APIQT_H

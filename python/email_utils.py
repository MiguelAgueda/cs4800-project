"""
Author: Miguel Agueda-Cabral
Project: Software Engineering Term Project, Baker's Ear.
         Available at https://gitlab.com/MiguelAgueda/cs4800-project
File: Python/db_utils.py
Date: 2 December 2020
Description:
    Email Utilities Class
    Implements base tools for communicating with customers via automated email.
"""

import datetime
import os
import random
import ssl
import smtplib


class EmailTools:
    """ Handles connecting to Google SMTP mail server and sending of mail."""
    def __init__(self):
        self.bot_email = "elnenescookiebot@gmail.com"
        self.bot_pass = os.environ.get("CS4800_EMAIL_PASS", "")
        if len(self.bot_pass) == 0:
            print("No Bot Password Found")
        # self.server = smtplib.SMTP_SSL("smtp.gmail.com", 465, ssl.create_default_context())
        # self.server.login(self.bot_email, self.bot_pass)
    
    def reformat_order(self, order):
        order["date"] = datetime.datetime.today().strftime("%H:%M %b %d %Y")
        order_content_string = ""
        for item in order["order"]:
            order_content_string += F"\t\t{item['quantity']} {item['cookie']} \n"
        
        order["order"] = order_content_string

        signature_variety = ["great", "excellent", "wonderful"]
        order["signature_variety"] = random.choice(signature_variety)

        return order
    
    def send_order_confirm(self, order_info):
        order_info = self.reformat_order(order_info)
        message = F"""\
Subject: El Nene's Cookie Order: {order_info["_id"]}

 Hello, {order_info["name"]}, 

You are receiving this email as a confirmation for your El Nene's order.
Here is a digital receipt for your order:
\tOrder Number: {order_info["_id"]}
\tDate of Order: {order_info["date"]}
\tOrder: \n{order_info["order"]}
            
When your order is ready, you will receive another automated email.

Have a {order_info["signature_variety"]} day!

    - El Nene's Cookie Bot
            """
        
        self.send_secure(order_info["email"], message)
        return True

    def send_order_ready(self, order_info):
        message = F"""\
            Subject: El Nene's Cookie Order: {order_info["_id"]} is ready!

                Hello, {order_info["name"]}, 
            
            Your El Nene's cookie order is ready!


            Have a {order_info["signature_variety"]} day!

                - El Nene's Cookie Bot
            """
        
        self.send_secure(order_info["email"], message)
        return True
        
    def send_secure(self, to, message):
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=ssl.create_default_context()) as server:
            server.login(self.bot_email, self.bot_pass)
            server.sendmail(self.bot_email, to, message)

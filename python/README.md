<!--
Author: Miguel Agueda-Cabral
Project: Software Engineering Term Project, Baker's Ear.
         Available at https://gitlab.com/MiguelAgueda/cs4800-project
File: Python/README.md
Date: 27 November 2020
-->

# Baker's Ear: API Documentation

## Setting Up MongoDB

[This guide](https://docs.mongodb.com/manual/administration/install-community/)
covers the installation of a MongoDB server for local use.

You will also need to have the following Python packages installed:

- [`pymongo`](https://pypi.org/project/pymongo/)
- [`Flask`](https://flask.palletsprojects.com/en/1.1.x/)

To install the above packages, the `python/requirements.txt` file may be used.

```bash
pip install -r python/requirements.txt
```

---

## Environment Variables

### Using MongoDB cloud service.

The API requires read and write access to the cloud database.
In order for this to occur securely, the system being used to run the server
must have the proper environment variables set up.

These variables are:

- `CS4800_DB_USER`: Database username.
- `CS4800_DB_PASS`: Database password.

Attempting to connect to the cloud service, without the proper environment variables,
will yield errors to the console.

### Using local MongoDB database.

The environment variables do not need to be set when working with a local database.
This allows for local testing without sharing usernames and passwords.

```python
""" Set up a local database connection."""
from db_utils import DBTools

# Initialize a database connection.
db = DBTools()
# Instruct DBTools instance to use local database.
db_tools.local = True  # This is default behavior.
```

---

# API Documentation

All API calls are available from the `/api/{call}` URL extension.

## POST Methods

### `submit_order` accepts a JSON order, submits order to database.

    Parameters:
        `order`: Order dictionary, JSON formatted.
    Returns:
        `response`: Dictionary containing status of order creation.

### `get_order_status` accepts an order ID, returns order.

    Parameters:
        `_id`: ObjectID of order to return.
    Returns:
        `order`: Order dictionary.

### `update_order` accepts an updated version of an existing order, updates the order in the database.

    Parameters:
        `_id`: ObjectID of order to update.
        `order`: Dictionary containing updated order fields.
    Returns:
        `response`: Status of order update.

### `delete` accepts an existing order _id, deletes the order with corresponding _id from database.

    Parameters:
        `_id`: ObjectID of order to delete.
    Returns:
        `response`: Status of order deletion.

### `update_inventory` accepts an updated version of store inventory, updates the inventory database.

    Parameters:
        `inventory`: Dictionary containing updated inventory fields.
            Any inventory fields left undefined will not be changed in the database.
    Returns:
        `response`: Status of order update.

## GET Methods

### `get_orders` returns _all_ orders that are available in the database.

    Returns:
        `orders`: Numbered dictionary of all existing orders.
            {
                1: {...},
                2: {...},
                ...
            }

### `get_inventory` returns existing inventory dictionary. 

    Returns:
        `inventory`: Dictionary containing current inventory. 
            When inventory is not defined, as will be during setup, a FAILURE message is returned.
            To fix this error, the inventory must first be updated. 

### `ping` returns "Pong!" as a simple check for API access.


# Testing the API

To test the API and it's connection to the database, use the following commands in a terminal.

Required tools: [cURL](https://curl.se/)

### Submit an order.

To test the API on a Linux machine, 

```bash
curl -X POST -H "Content-Type: application/json" -d '{"name": "No Name", "email": "cookie@buyer.com", "delivery": "False", "order": [{"cookie": "chocolate chip", "quantity": "10"}]}' http://127.0.0.1:5000/api/submit_order
```

To test on Windows,

```powershell
Invoke-WebRequest -Uri http://127.0.0.1:5000/api/submit_order -Method 'POST' -ContentType 'application/json' -Body '{"name": "No Name", "email": "cookie@buyer.com", "delivery": "False", "order": [{"cookie": "chocolate chip", "quantity": "10"}]}'
```

If the API is working properly, a success message will be returned via JSON.

```bash
~$ {"status":"success"}
```

### Get all existing orders.

Linux, 

```bash
curl -X GET http://127.0.0.1:5000/api/get_orders
```

Windows,

```powershell
Invoke-WebRequest -Uri http://127.0.0.1:5000/api/get_orders -Method 'GET'
```

### Update an order.

To update an order, you will need to use an existing order's `_id` attribute.
The `_id`s of existing orders can be retrieved with the method above.

Linux, 

```bash
curl -X POST -H "Content-Type: application/json" -d '{"_id": "ID GOES HERE", "order": {"name": "CHANGED"}}' http://127.0.0.1:5000/api/update_order
```

Windows,

```powershell
Invoke-WebRequest -Uri http://127.0.0.1:5000/api/update_order -Method 'POST' -ContentType 'application/json' -Body '{"_id": "ID GOES HERE", "order": {"name": "CHANGED"}}'
```

### Update Inventory

Linux,

```bash
curl -X POST -H "Content-Type: application/json" -d '{"inventory": {"chocolate chip":0, "brownie":0, "macadamia":0}}' http://127.0.0.1:5000/api/update_order
```

Windows,

```powershell
Invoke-WebRequest -Uri http://127.0.0.1:5000/api/update_inventory -Method 'POST' -ContentType 'application/json' -Body '{"inventory": {"chocolate chip":0, "brownie":0, "macadamia":0}}'
```

"""
Author: Miguel Agueda-Cabral
Project: Software Engineering Term Project, Baker's Ear.
         Available at https://gitlab.com/MiguelAgueda/cs4800-project
File: Python/main.py
Date: 27 November 2020
Description:
    Main Application File
    A Flask server is initialized in this app.
    This server will handle API requests for interfacing with a database.
"""
import flask
from flask import Flask, jsonify, request, render_template, send_file
from flask_cors import CORS

from db_utils import DBTools
from email_utils import EmailTools


# Generic `success` message to return to callers.
SUCCESS = {"status": "success"}

# Generic `failure` message to return to callers.
FAILURE = {"status": "failure"}

# Instantiate email server connection.
mailer = EmailTools()

# instantiate database connection.
db = DBTools()
# db.local = True

# When connecting to a live database:
user_var_name = "CS4800_DB_USER"
pass_var_name = "CS4800_DB_PASS"
db.set_env_vars(user_var_name, pass_var_name)
db.local = False

# instantiate the app
app = Flask(__name__, static_folder="./templates/")

# enable CORS
CORS(app, resources={r'/api/*': {'origins': '*'}})

# Start API routing.
@app.route('/')
def index():
    return "<h1>Welcome to Baker's Ear!</h1>"
    # return render_template("index.html")

@app.route("/api/submit_order", methods=["POST"])
def submit_order():
    """ Submit order to the database."""

    response = FAILURE  # Create response dict.
    if request.method == "POST":  # Check for POST before collecting JSON.
        order = request.get_json()  # Collect JSON input.
        if not order:
            print("\nERROR: Order is of type NONE.\n")
            pass  # Do nothing, response is already FAILURE.

        else: 
            order["status"] = "unfulfilled"
            for ordered_item in order["order"]:
                ordered_item["quantity"] = int(ordered_item["quantity"])

            status, order_id = db.create(order)  # Submit order, wait for status.
            if status:
                response = SUCCESS  # Order submitted successfully.
                db.decrement_inventory(order)
                order["_id"] = order_id
                mailer.send_order_confirm(order)
        
            else:
                response = FAILURE

    return jsonify(response)


@app.route('/api/get_orders', methods=["GET"])
def get_orders():
    """ Retrieve orders from database."""

    orders = db.read_all()

    return jsonify(orders)


@app.route('/api/get_order_status', methods=["POST"])
def get_order_status():
    """ Get order status, of requested order, from the database."""

    post_data = request.get_json()
    print(post_data)
    order = db.read(post_data["_id"])

    return jsonify(order)


@app.route('/api/update_order', methods=["POST"])
def update_order():
    """ Update order with new order details."""

    response = FAILURE
    post_data = request.get_json()
    if db.update(post_data["_id"], post_data["order"]):
        response = SUCCESS

    return jsonify(response)


@app.route('/api/delete', methods=["POST"])
def delete():
    """ Delete order using existing order _id."""

    response = FAILURE
    post_data = request.get_json()
    if db.delete(post_data["_id"]):
        response = SUCCESS

    return jsonify(response)


@app.route('/api/update_inventory', methods=["POST"])
def update_inventory(data=None):
    """ Update inventory with new inventory and quantities."""

    response = FAILURE
    if not data:
        data = request.get_json()
    print("POST DATA HERE", data)
    updated_inventory = data["inventory"]
    if db.update_inventory(updated_inventory):
        response = SUCCESS
    
    return jsonify(response)


@app.route('/api/get_inventory', methods=["GET"])
def get_inventory():
    """ Get current inventory."""
    inventory = db.get_inventory()
    try:
        inventory["_id"] = str(inventory["_id"])
    except KeyError:
        inventory = FAILURE
    return jsonify(inventory)


@app.route("/api/ping", methods=["GET"])
def ping_pong():
    """ A must-have sanity check."""
    return jsonify('Pong!')

if __name__ == '__main__':
    app.run(threaded=True, port=5000)

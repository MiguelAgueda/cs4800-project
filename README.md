
CS4800 Group Project
=======
# El Nene's Cookie Management System - Baker's Ear

## Report 1

[Report 1](https://docs.google.com/document/d/19KaT2Z7mH7Kd220nTdaxJEcF4Rg86h1dyx6N7hgGus8/edit?usp=sharing)

## Project Proposal

Our project proposal can be found 
[here](https://docs.google.com/document/d/10PDr3KvpaofCYsLXjhWYz_V8ta8YTABEO4li9hjwXKs/edit?usp=sharing)

## Details

Backend written in C++ and QML (QtQuick). Frontend JS website with Python/Flask REST API.
